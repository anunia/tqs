package com.example.air_quality_api.Api;

import com.example.air_quality_api.API.Location;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LocationTest {
    private Location coords;

    @BeforeEach
    void setUp() {
        coords = new Location(2.253778, 2.253778);
    }

    @Test
    void getLon() {
        assertEquals(2.253778,coords.getLon());
    }

    @Test
    void getLat() {
        assertEquals(2.253778,coords.getLat());
    }
}