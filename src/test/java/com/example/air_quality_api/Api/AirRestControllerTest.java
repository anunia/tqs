package com.example.air_quality_api.Api;

import com.example.air_quality_api.API.InfoService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.http.MediaType;
import org.mockito.internal.verification.VerificationModeFactory;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.mockito.Mockito.*;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest
public class AirRestControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private InfoService infoService;

    @Test
    void getInfoRequest() {
    }

    @Test
    void getCache() throws Exception {
        when(infoService.getHits()).thenReturn(1);
        when(infoService.getMisses()).thenReturn(2);
        when(infoService.getRequests()).thenReturn(3);

        mockMvc.perform(get("/api/cache")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.*", hasSize(3)))
                .andExpect(jsonPath("$.Requests", is("3")))
                .andExpect(jsonPath("$.Misses", is("2")))
                .andExpect(jsonPath("$.Hits", is("1")))
        ;
        verify(infoService, times(1)).getHits();
        verify(infoService, times(1)).getRequests();
        verify(infoService, times(1)).getMisses();
        reset(infoService);
    }
}

