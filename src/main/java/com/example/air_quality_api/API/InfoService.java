package com.example.air_quality_api.API;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;


@Service
public class InfoService implements InfoServiceInterface{
        @Value("${breezometer.api}")
        private String url;

        @Value("${breezometer.key}")
        private String key;

        @Value("${breezometer.features}")
        private String features;

        @Autowired
        private AirRepository airRepository;

        private final HttpClient httpClient = HttpClient.newBuilder()
                .version(HttpClient.Version.HTTP_2)
                .build();

        public String sendGET(String lat, String lon) throws IOException, InterruptedException {
                HttpRequest request = HttpRequest.newBuilder()
                        .GET()
                        .uri(URI.create(
                                this.url+
                                        "lat="+lat +
                                        "&lon="+lon +
                                        "&key="+this.key+
                                        "&features=" + this.features))
                        .setHeader("User-Agent", "Java 11 HttpClient Bot")
                        .build();

                HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

                return response.body();
        }


        public InfoRequest getAirQualityByLocal(double lat, double lon) {
                InfoRequest infoRequest = airRepository.getData(lat, lon);
                if (infoRequest == null){
                        try {
                                String getResult = sendGET("" + lat, "" + lon);
                                infoRequest = new InfoRequest(getResult);

                                this.airRepository.putData(lat, lon, getResult);
                        } catch (Exception e){
                                return null;
                        }
                }

                return infoRequest;
        }

        public int getMisses() {
                return airRepository.getMiss();
        }
        public int getHits() {
                return airRepository.getHit();
        }
        public int getRequests() {
                return airRepository.getHit() + airRepository.getMiss();
        }

        public void clearCache() {
                airRepository.clear();
        }

}
