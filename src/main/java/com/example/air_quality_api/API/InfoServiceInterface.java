package com.example.air_quality_api.API;

import com.example.air_quality_api.API.InfoRequest;

public interface InfoServiceInterface {
    InfoRequest getAirQualityByLocal(double lon, double lat);
    int getMisses();
    int getHits();
    int getRequests();
    void clearCache();
}
