package com.example.air_quality_api.API;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RestController
@RequestMapping("/api")

public class AirRestController {
    @Autowired
    private InfoService InfoServiceInterface;

    @GetMapping("/info")
    public ResponseEntity<InfoRequest> getInfoRequest(@RequestParam double lat, @RequestParam double lon) {
        return ResponseEntity.ok(InfoServiceInterface.getAirQualityByLocal(lat, lon));
    }

    @GetMapping("/cache")
    public ResponseEntity<HashMap<String, String>> getCache(){
        HashMap<String, String> cache = new HashMap<>();
        cache.put("Requests", ""+InfoServiceInterface.getRequests());
        cache.put("Hits", ""+InfoServiceInterface.getHits());
        cache.put("Misses", ""+InfoServiceInterface.getMisses());
        return new ResponseEntity<>(cache, HttpStatus.OK);
    }
    @GetMapping("/clear")
    public void clearCache(){
        InfoServiceInterface.clearCache();
    }
}
