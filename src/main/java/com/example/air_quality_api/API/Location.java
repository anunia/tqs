package com.example.air_quality_api.API;

import java.util.Objects;

public class Location {
    private double lon;
    private double lat;

    public Location(double lat, double lon) {
        this.lon = lon;
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public double getLat() {
        return lat;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Location)) return false;
        Location airCoord = (Location) o;
        return Double.compare(airCoord.lon, lon) == 0 &&
                Double.compare(airCoord.lat, lat) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(lon, lat);
    }
}
