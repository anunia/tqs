package com.example.air_quality_api.API;


import org.springframework.stereotype.Repository;

import java.util.HashMap;

@Repository
public class AirRepository {
    public HashMap<Location, InfoRequest> cache;
    private int ttl;
    private int hit;
    private int miss;

    public AirRepository(int ttl) {
        this.cache = new HashMap<>();
        this.ttl = ttl;
        this.hit = 0;
        this.miss = 0;
    }

    // Default 10 minute time-to-live
    public AirRepository() {
        this(600000);
    }

    // Getters

    public InfoRequest getData(double lat, double lon){
        Location location = new Location(lat, lon);

        if (cache.containsKey(location) && System.currentTimeMillis() < cache.get(location).getRequestDate() + this.ttl){
            this.hit++;
            return cache.get(location);
        }
        return null;
    }

    public int getMiss() {
        return this.miss;
    }

    public int getHit() {
        return this.hit;
    }

    public void clear(){
        this.cache.clear();
        this.miss = 0;
        this.hit = 0;
    }

    public InfoRequest putData(double lat, double lon, String data) {
        this.miss++;
        InfoRequest infoRequest = new InfoRequest(data);
        this.cache.put(new Location(lat,lon), infoRequest);
        return infoRequest;
    }
}
