package com.example.air_quality_api.API.Polution_types;

public class PM10 extends Pollutants {
    public PM10(BAQI pm10BAQI, double pollutantConcentration) {
        super(pm10BAQI, "Inhalable particulate matter (<10um)", "PM10", pollutantConcentration, "ug/m3");
    }
}
