package com.example.air_quality_api.API.Polution_types;

public class NO2 extends Pollutants {
    public NO2(BAQI no2BAQI, double pollutantConcentration) {
        super(no2BAQI, "Nitrogen Dioxide", "NO2", pollutantConcentration, "ppb");
    }
}
