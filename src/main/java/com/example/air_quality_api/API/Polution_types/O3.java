package com.example.air_quality_api.API.Polution_types;

import com.example.air_quality_api.API.Polution_types.BAQI;
import com.example.air_quality_api.API.Polution_types.Pollutants;

public class O3 extends Pollutants {
    public O3(BAQI o3BAQI, double pollutantConcentration) {
        super(o3BAQI, "Ozone", "O3", pollutantConcentration, "ppb");
    }
}
