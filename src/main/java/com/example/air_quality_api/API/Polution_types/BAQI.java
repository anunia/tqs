package com.example.air_quality_api.API.Polution_types;

import java.util.Objects;

public class BAQI {
    private int aqi;
    private String color;
    private String category;

    public BAQI(int aqi, String color, String category) {
        this.aqi = aqi;
        this.color = color;
        this.category = category;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BAQI)) return false;
        BAQI baqi = (BAQI) o;
        return aqi == baqi.aqi &&
                Objects.equals(color, baqi.color) &&
                Objects.equals(category, baqi.category);
    }

    @Override
    public String toString() {
        return "BAQI{" +
                "aqi=" + aqi +
                ", color='" + color + '\'' +
                ", category='" + category + '\'' +
                '}';
    }


    public String getCategory() {
        return this.category;
    }
    public String getColor() {
        return this.color;
    }
    public int getAqi() {
        return this.aqi;
    }

    @Override
    public int hashCode() {
        return Objects.hash(color,aqi,category);
    }
}