package com.example.air_quality_api.API.Polution_types;

public class PM25 extends Pollutants {
    public PM25(BAQI pm25BAQI, double pollutantConcentration) {
        super(pm25BAQI, "Fine particulate matter (<2.5um)", "PM2.5", pollutantConcentration, "ug/m3");
    }
}
