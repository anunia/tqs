package com.example.air_quality_api.API.Polution_types;

public class CO extends Pollutants {
    public CO(BAQI coBAQI, double concentration) {
        super(coBAQI, "Carbon Monoxide", "CO", concentration, "ppb");
    }
}
