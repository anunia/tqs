package com.example.air_quality_api.API.Polution_types;

public class SO2 extends Pollutants {
    public SO2(BAQI so2BAQI, double pollutantConcentration) {
        super(so2BAQI, "Sulfur Dioxide", "SO2", pollutantConcentration, "ppb");
    }
}
